# Вставляйте решение под задачей

# К просмотре обязательно ---- https://www.youtube.com/watch?v=zZBiln_2FhM

# Создайте массив состоящий из 5ти словарей
f = [
    { 'name':'ilya' },
    { 'lastname':'adjahunov'},
    {'age': 26},
    {'hobby': 'snowboard'},
    {'srok_obucheniya': 7}
]
# Создайте массив состоящий из 5ти массивов с любыми значениями
random = [[1, 2, 3],[a, b, c],[3, 4, 5],[f, c, g],[4, 1, 2]]


# Замените в ранее созданном массиве несколько значений
random = [[1, 2, 3],['a', 'b', 'c'],[3, 4, 5],['f', 'c', 'g'],[4, 1, 2]]
random[1] = [4 , 5, 6]
print(random)
# Создайте цикл для первого масссива
for i in f:
    print(i)


# Создайте цикл для второго масссива
for i in random:
    print(i)

# Создайте массив состоящий из 10 цифр от 0 и до 20
random = [8, 9, 1, 2, 3, 4, 5, 6, 7, 11]


# Пройдитесь циклоом по вышесозданному муссиву и выведите все чита которые больше 10ти
random = [8, 9, 1, 2, 3, 4, 5, 6, 7, 11]
for vivesti in random:
    if vivesti > 10:
        print(vivesti)

# Создайте словарь который содержит в себе все типы данных которые мы проходили (ключи на ваше усморение)
slovar = {
    'ciforki': 111,
    'bukovki': 'abc',
    'massivi': [1, 2, 3]
    'slovari': {'gladious': 'romashka'}
}


# Создайте словарь который содержит в себе 5 разных массивов
g = {
    'numbers' : [1,2,3],
    'strings' : ['a','b','c'],
    'numbers1' : [7,8,4],
    'bukvocifri' : ['a1', 9, 'q7'],
    'cifrobukvi' : ['one', 'two', 3]}
print(g)


# Создайте условия if / else
password = 123456
if password == password:
    print('OK, Welcome!')
else:
    print('Pswrd incorrect')


# Создайте условия if / elif / else
friend = {
        'name': 'yan',
        'age': 25,
        'moi kent': True
        }
if friend['name'] == 'arsen':
        print('zahodi')
elif friend['moi kent']:
        print('zahodi')
else:
        print('pshel von')


# Создайте условия if / elif / elif / else
friend = {
        'name': 'yan',
        'age': 25,
        'moi kent': True,
        'rieltor' : False
        }
if friend['name'] == 'arsen':
        print('zahodi')
elif friend['rieltor']:
        print('zahodi rielt')
else:
        print('pshel von')


# Создайте условия цикл который проходится по массиву в который содержит в себе условия if / elif / else
for x in friend:
    print('x')
friend = {
        'name': 'yan',
        'age': 25,
        'moi kent': True,
        'rieltor' : False
        }
if friend['name'] == 'arsen':
        print('zahodi')
elif friend['rieltor']:
        print('zahodi rielt')
else:
        print('pshel von')